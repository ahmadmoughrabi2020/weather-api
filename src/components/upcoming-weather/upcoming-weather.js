import React from 'react'
import "./upcoming-weather.css"
import storm from "../../img/weather-icons/storm.svg";
import clear from "../../img/weather-icons/clear.svg";
import drizzle from "../../img/weather-icons/drizzle.svg";
import fog from "../../img/weather-icons/fog.svg";
import rain from "../../img/weather-icons/rain.svg";
import snow from "../../img/weather-icons/snow.svg";
import  mostlycloudy from "../../img/weather-icons/mostlycloudy.svg";
import partlycloudy from "../../img/weather-icons/partlycloudy.svg";
import WeatherItem from '../weather-items/weather-item';


const UpcomingWeather = (props) => {
  
  return (
    <>
    <div className="upcoming__weather">
    <ul className="next__Weather" id="nextWeather">
       
      {props.Data.filter((item,i)=>i<7).map((postData,index)=>{
              
                const hrs = postData.dt_txt.split(' ')[1].substr(0,5);
                const temp = Math.round(postData.main.temp);
                const id =postData.weather[0].id; 
                let ImgArr = [];
                let dailyImageNameArr = [];
                const getImage=(id)=> {
                  let st = storm;
                  let dr = drizzle;
                  let ra = rain ;
                  let sn = snow;
                  let fo = fog;
                  let cl = clear;
                  let part = partlycloudy;
                  let most = mostlycloudy;
               
                  if (id <= 300) return  st
                  
                  if (300 <= id && id <= 499) return dr
                  
                  if (500 <= id && id <= 599) return  ra
                  
                  if (600 <= id && id <= 699) return  sn
                  
                  if (700 <= id && id <= 799) return  fo
                  
                  if (id === 800) return   cl
                  
                  if (id === 801) return  part
     
                  return  most
                 
                }
               for (let i = 0; i < 8; i++) {
                 dailyImageNameArr.push(postData.weather[0].id);
               }
               for(let i = 0 ;i <8;i++){
                  ImgArr.push(getImage(dailyImageNameArr[i]))
                }
                return(
                <WeatherItem key={postData.dt} hrs={hrs} image={ImgArr[7]} temp={temp}/>
                );
              
                })}
                 </ul>
                </div>
        </>     )
}            
export default UpcomingWeather;
