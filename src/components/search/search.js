import React,{ useState, useEffect } from "react";
import "./search.css"
import Weather from "../weather-header/weather-header";
class search extends React.Component {
  constructor(props) {
      super(props);
      this.state = {City: "Beirut"};
     
      this.handleCityChange = this.handleCityChange.bind(this);
      this.onClick = this.onClick.bind(this);
          }    
      handleCityChange(event){
        this.setState({City: event.target.value});
      }     
      onClick(){
        this.props.getWeather(this.state.City);
      }      
    render(){
    return (
      
    <div className="search__bar">
    <input
    placeholder="Type in a City Name"
    type="text"
    id="inputCity"
    className="inputCity"
    ize="20"
    maxLength="20"
    value={this.state.City}
    onChange={this.handleCityChange}
    />
    <button id="findWeather" className="findWeather" onClick={this.onClick}  >Find Weather</button>
    </div>
     );
   }
}
export default search;