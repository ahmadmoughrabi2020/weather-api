import React from "react";
const WeatherItem = (props) => {
   
  return (
    <li >
      <p>{props.hrs} </p>
      <img className="new__Weather" src={props.image} alt="storm icon" />
      <p>{props.temp} &#8451;</p>
    </li>
  );
};
export default WeatherItem;
