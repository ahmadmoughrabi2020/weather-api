import React from 'react'
import "./weather-header.css"

class WeatherHeader extends React.Component{

  render(){
  return (
    <div  className="weather_header">

    <img className="logo" src={this.props.dailyImage} fill="white"  alt="storm icon" />
    
    <p className="weatherState" id="weatherState">
    {" "}
    {this.props.description}
    </p>
    
    <h2 className="temperature" id="temperature">
     Temperature {this.props.minTempreture} &#8451; to {this.props.maxTempreture} &#8451;{" "}
    </h2>
    <div className="preHum">
    <p className="humidity" id="humidity">
     Humidity{" "}
    </p>
    <p className="humValue" id="humValue">
    {" "}
    {this.props.humidity}%
    </p>
    <p className="pressure" id="pressure">
     Pressure{" "}
    </p>
    <p className="pressValue" id="pressValue">
    {this.props.pressure}
    </p>
    </div>
    
    </div> 
    );

  }  
}
export default WeatherHeader;
