import './App.css';
import React, { Component} from "react";
import NetworkDetector from './HOC/NetworkDetector.jsx';
import WeatherHeader from "./components/weather-header/weather-header";
import UpcomingWeather from "./components/upcoming-weather/upcoming-weather";
import Search from "./components/search/search";
import storm from "./img/weather-icons/storm.svg";
import clear from "./img/weather-icons/clear.svg";
import drizzle from "./img/weather-icons/drizzle.svg";
import fog from "./img/weather-icons/fog.svg";
import rain from "./img/weather-icons/rain.svg";
import snow from "./img/weather-icons/snow.svg";
import  mostlycloudy from "./img/weather-icons/mostlycloudy.svg";
import partlycloudy from "./img/weather-icons/partlycloudy.svg";

class  App extends Component {
  constructor(props) {

    super(props);
        this.state = {
        city: "",
        dailyImageName: [],
        jsonResponse:[],
        dailyTempreture: [],
        dailyId:[],
        dailyMaxTemreture: [],
        dailyMinTemreture:[],
        description: "",
        data:[],
        humidity:"",
        pressure:"",
        id:[],
        postData:[],
        hrs:[],
        img:""
        }  
      this.getWeather = this.getWeather.bind(this);
    }
    
     getImage=(id)=> {
             let st = storm;
             let dr = drizzle;
             let ra = rain ;
             let sn = snow;
             let fo = fog;
             let cl = clear;
             let part = partlycloudy;
             let most = mostlycloudy;
          
             if (id <= 300) return  st
             
             if (300 <= id && id <= 499) return dr
             
             if (500 <= id && id <= 599) return  ra
             
             if (600 <= id && id <= 699) return  sn
             
             if (700 <= id && id <= 799) return  fo
             
             if (id === 800) return   cl
             
             if (id === 801) return  part

             return  most

      }


     async getWeather(cityName){
       // save the city name
       this.setState({city: cityName});
     
      console.log(cityName);
      const response = await fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&cnt=8&units=metric&appid=a79b09f3ba3791e0460d5c324c399857`);
       const jsonResponse = await response.json();
       if (jsonResponse.list) {
         

         //save data
         let Data = [];
         for(let i = 0 ; i < 8 ; i++){
           Data.push(jsonResponse.list[i]);
         }
         Data = Data.map(data => {
           return data;
         });
        
         this.setState({data:Data})
         
         // save the daily tempreture
         let dailyTempArr = [];
         for(let i = 0 ; i < 8 ; i++){
          dailyTempArr.push(jsonResponse.list[i].main.temp);  
         }
         // round all the values of the array
         dailyTempArr = dailyTempArr.map(temp => {
           return Math.round(parseFloat(temp)).toString();
         });

        
         this.setState({ dailyTempreture: dailyTempArr });

         // save the daily max tepreture - for now we only use the current max tempreture (0)
         let dailyMaxTempArr = [];
         for(let i = 0 ; i < 8 ; i++){
          dailyMaxTempArr.push(jsonResponse.list[i].main.temp_max);  
         }
         dailyMaxTempArr = dailyMaxTempArr.map(temp => {
           return Math.round(parseFloat(temp)).toString();
         });
         
         this.setState({ dailyMaxTemreture: dailyMaxTempArr });

         // save the daily min tepreture - for now we only use the current max tempreture (0)
         let dailyMinTempArr = [];
         for (let i = 0; i < 8; i++) {
           dailyMinTempArr.push(jsonResponse.list[i].main.temp_min);
         }
         dailyMinTempArr = dailyMinTempArr.map(temp => {
           return Math.round(parseFloat(temp)).toString();
         });
         
         this.setState({ dailyMinTemreture: dailyMinTempArr });
         //humidity
         let humidity = jsonResponse.list[0].main.humidity;
         
         this.setState({ humidity: jsonResponse.list[0].main.humidity });
         //pressure
         let pressure = jsonResponse.list[0].main.pressure;
        
         this.setState({ pressure: jsonResponse.list[0].main.pressure });
         // save the main id in order to determine the image
         let dailyImageNameArr = [];
         for (let i = 0; i < 8; i++) {
          dailyImageNameArr.push(jsonResponse.list[i].weather[0].id);
        }
         this.setState({ dailyImageName: dailyImageNameArr });
       
         // save the current description
         this.setState({ description: jsonResponse.list[0].weather[0].description });
         //hours
         let hrs = "";
         let hrsArr = [];
         for (let i = 0; i < 8; i++) {
           hrsArr.push(jsonResponse.list[i].dt_txt);
         }
         hrsArr = hrsArr.map(hrs => {
           return hrs.split(' ')[1].substr(0, 5);
         });
        
         this.setState({ hrs: hrsArr });
        
         // ID array for Images
         let ImgArr=[];
         for(let i = 0 ; i <dailyImageNameArr.length ; i++) {
          ImgArr.push(this.getImage(dailyImageNameArr[i]))
          }
         
         this.setState({img:ImgArr});

         const myElement = document.getElementById("app");
         let colorImg = "";
         
       
         console.log(this.state.img);
         if(this.state.img[0] == "/static/media/mostlycloudy.8e5d988e.svg"){
           console.log("mostlycloudy");
           colorImg="mostlycloudy";

           function emphasise(element, time = 5) {
            element.classList.add("mostlycloudy");
            setTimeout(function() {
              element.classList.remove("mostlycloudy")
            }, time * 1000);
          }

           emphasise(myElement, 5);
        }
        if(this.state.img[0]=="/static/media/rain.d6fe6255.svg"){

          console.log("rain");
          colorImg="rain";

          function emphasise(element, time = 5) {
           element.classList.add("rain");
           setTimeout(function() {
             element.classList.remove("rain")
           }, time * 1000);
         }

          emphasise(myElement, 5);

        }

        if(this.state.img[0]=="/static/media/snow.041ad5ed.svg"){

          console.log("snow");
          colorImg="snow";

          function emphasise(element, time = 5) {
           element.classList.add("snow");
           setTimeout(function() {
             element.classList.remove("snow")
           }, time * 1000);
         }

          emphasise(myElement, 5);

        }
        if(this.state.img[0]=="/static/media/clear.a0404ecc.svg"){

          console.log("clear");
          colorImg="clear";

          function emphasise(element, time = 5) {
           element.classList.add("clear");
           setTimeout(function() {
             element.classList.remove("clear")
           }, time * 1000);
         }

          emphasise(myElement, 5);

        }





      } 
       else alert('Enter Valid City');                  
}
componentWillMount() {
this.getWeather("Beirut");
this.state.cityName="Beirut";   

}
render(){
  return (
    <div id = "app" className="app">
        <header className="app__header">

          <Search   cityName={this.state.cityName} getWeather={this.getWeather} />

        </header>
        <main className="app__main">

            <div className="container">
           
              <WeatherHeader humidity={this.state.humidity} pressure={this.state.pressure} description={this.state.description} minTempreture={this.state.dailyMinTemreture[0]}  maxTempreture={this.state.dailyMaxTemreture[0]}
               dailyImage={this.state.img[0]}  tempreture={this.state.dailyTempreture[0]} />
              <UpcomingWeather ImgArr={this.state.img} Data={this.state.data}  dailyTempArr={this.state.dailyTempreture} hrs= {this.state.hrs }/>
            </div>
          </main>
      
    </div>
  );
 }
}
export default NetworkDetector(App);
